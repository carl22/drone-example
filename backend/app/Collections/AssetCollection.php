<?php
namespace App\Collections;

use App\Interfaces\Contracts\IAssetHandler;
use Illuminate\Database\Eloquent\Collection;

class AssetCollection extends Collection {
    public function toResourceAsArray() {
        return $this->map(function(IAssetHandler $handler) {
            return $handler->toResourceAsArray();
        })->values();
    }
}