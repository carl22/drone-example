<?php
namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Support\Arrayable;

abstract class BaseApiException extends Exception implements Arrayable {

    public function toArray() {
        return [
            'error'=>$this->getMessage(),
            'code'=>$this->getCode()
        ];
    }

    public function getStatusCode() {
        return $this->getCode();
    }
}