<?php
namespace App\Exceptions;


class AuthenticationException extends BaseApiException{
    protected $code = 403;
}