<?php

namespace App\Models;

use App\Interfaces\Contracts\IAuthenticatableToken;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements IAuthenticatableToken
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function companies() {
        return $this->hasManyThrough(Company::class, UserCompany::class,'user_id','id','id','company_id');
    }
    public function defaultCompany() {
        return $this->belongsTo(Company::class,'default_company_id', 'id');
    }
    public function missions() {
        return $this->hasMany(Mission::class,'created_by_user_id', 'id');
    }

    public function isInCompany(Company $company) {
        if(is_null($this->defaultCompany)){
            return false;
        }
        return $this->defaultCompany->id === $company->id;
    }
}
