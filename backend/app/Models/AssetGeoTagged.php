<?php

namespace App\Models;

use App\Collections\AssetCollection;
use App\Interfaces\Contracts\IAssetHandler;
use App\Interfaces\Services\IAssetService;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AssetGeoTagged extends Model implements IAssetHandler
{
    use HasFactory, ValidatesRequests, SpatialTrait;
    public const ASSET_KEY = 'geo_tagged_photo';

    protected $table='geotag_photo_assets';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'asset_id',
        'name',
        'drive',
        'path',
        'type',
        'location'
    ];

    protected $spatialFields = [
        'location'
    ];


    public function asset() {
        return $this->belongsTo(Asset::class,'asset_id','id');
    }

    public function validateCreateRequest(Request $request, Asset $asset, IAssetService $service){
        $this->validate($request,[
            'name'=>'required|string|max:255',
            'image'=>'required|image',
            'latitude'=>'required|numeric',
            'longitude'=>'required|numeric'
        ]);
    }
    public function validateUpdateRequest(Request $request, Asset $asset, IAssetService $service){
        $this->validate($request,[
            'name'=>'required|string|max:255',
            'image'=>'image',
            'latitude'=>'required_with:longitude|numeric',
            'longitude'=>'required_with:latitude|numeric'
        ]);
    }
    public function handleCreate(Request $request, Asset $asset, IAssetService $service){
        $path = $request->file('image')->store('/',$service->getStorageDriver());
        $pathInfo = pathinfo($path);
        $this->asset_id = $asset->id;
        $this->name = $request->get('name');
        $this->drive = $service->getStorageDriver();
        $this->path = $path;
        $this->type = $pathInfo['extension'];
        $this->location = new Point($request->get('latitude'), $request->get('longitude'));
        $this->saveOrFail();
        return $this;
    }
    public function handleUpdate(Request $request, IAssetService $service){
        if($request->hasFile('image')) {
            if(Storage::disk($this->drive)->exists($this->path)) {
                Storage::disk($this->drive)->delete($this->path);
            }
            $path = $request->file('image')->store('/',$service->getStorageDriver());
            $pathInfo = pathinfo($path);
            $this->drive = $service->getStorageDriver();
            $this->path = $path;
            $this->type = $pathInfo['extension'];
        }
        if($request->has('name')) {
            $this->name = $request->get('name');
        }
        if($request->has('longitude') && $request->has('latitude')){
            $this->location = new Point($request->get('latitude'), $request->get('longitude'));
        }
        $this->saveOrFail();
        return $this;
    }
    public function handleDelete(Request $request, IAssetService $service){
        if(Storage::disk($this->drive)->exists($this->path)) {
            Storage::disk($this->drive)->delete($this->path);
        }
        return $this->asset->delete();
    }
    
    public function toResourceAsArray(){
        return [
            'id'=>$this->asset_id,
            'name'=>$this->name,
            'path'=>Storage::disk($this->drive)->url($this->path),
            'type'=>$this->type,
            'longitude'=>$this->location->getLng(),
            'latitude'=>$this->location->getLat(),
            'public'=> $this->asset->public
        ];
    }

    public function toVueDetailData(): array{
        return [
            'key'=> self::ASSET_KEY,
            'formComponent'=>'FormAssetGeoTaggedPhoto',
            'viewComponent'=>'AssetViewGeoTaggedPhoto',
            'collectionComponent'=>'AssetCollectionGeoTaggedPhoto',
            'name'=>'Geo Tagged Photo',
            'namePlural'=>'Geo Tagged Photos'
        ];
    }

    public function newCollection(array $models = [])
    {
        return new AssetCollection($models);
    }
    
}
