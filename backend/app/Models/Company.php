<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model 
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'consented_at',
        'consented_by',
        'confirmed_certifications_at',
        'confirmed_certifications_by'
    ];


    protected $casts = [
        'consented_at'=> 'datetime',
        'confirmed_certifications_at'=>'datetime',
        'consented_by'=>'integer',
        'confirmed_certifications_by'
    ];

    public function users() {
        return $this->hasManyThrough(User::class, UserCompany::class,'company_id','id','id','user_id');
    }

    public function consentedUser() {
        return $this->belongsTo(User::class,'consented_by','id');
    }

    public function confirmedCertificationUser() {
        return $this->belongsTo(User::class,'confirmed_certifications_by','id');
    }

    public function operationCountries() {
        return $this->hasManyThrough(Country::class, CompanyOperationCountry::class,'company_id','id','id','country_id');
    }

    public function missions() {
        return $this->hasMany(Mission::class,'company_id', 'id');
    }

}
