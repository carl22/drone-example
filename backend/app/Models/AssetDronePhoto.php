<?php

namespace App\Models;

use App\Collections\AssetCollection;
use App\Interfaces\Contracts\IAssetHandler;
use App\Interfaces\Services\IAssetService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AssetDronePhoto extends Model implements IAssetHandler
{
    use HasFactory, ValidatesRequests;
    public const ASSET_KEY = 'drone_photo';

    protected $table='drone_photo_assets';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'asset_id',
        'name',
        'drive',
        'path',
        'type'
    ];

    public function asset() {
        return $this->belongsTo(Asset::class,'asset_id','id');
    }


    public function validateCreateRequest(Request $request, Asset $asset, IAssetService $service){
        $this->validate($request,[
            'name'=>'required|string|max:255',
            'image'=>'required|image'
        ]);
    }
    public function validateUpdateRequest(Request $request, Asset $asset, IAssetService $service){
        $this->validate($request,[
            'name'=>'required|string|max:255',
            'image'=>'image'
        ]);
    }
    public function handleCreate(Request $request, Asset $asset, IAssetService $service){
        $path = $request->file('image')->store('/',$service->getStorageDriver());
        $pathInfo = pathinfo($path);
        $this->asset_id = $asset->id;
        $this->name = $request->get('name');
        $this->drive = $service->getStorageDriver();
        $this->path = $path;
        $this->type = $pathInfo['extension'];
        $this->saveOrFail();
        return $this;
    }
    public function handleUpdate(Request $request, IAssetService $service){
        if($request->hasFile('image')) {
            if(Storage::disk($this->drive)->exists($this->path)) {
                Storage::disk($this->drive)->delete($this->path);
            }
            $path = $request->file('image')->store('/',$service->getStorageDriver());
            $pathInfo = pathinfo($path);
            $this->drive = $service->getStorageDriver();
            $this->path = $path;
            $this->type = $pathInfo['extension'];
        }
        if($request->has('name')) {
            $this->name = $request->get('name');
        }
        $this->saveOrFail();
        return $this;
    }
    public function handleDelete(Request $request, IAssetService $service){
        if(Storage::disk($this->drive)->exists($this->path)) {
            Storage::disk($this->drive)->delete($this->path);
        }
        return $this->asset->delete();
    }
    public function toResourceAsArray(){
        return [
            'id'=>$this->asset_id,
            'name'=>$this->name,
            'path'=>Storage::disk($this->drive)->url($this->path),
            'type'=>$this->type,
            'public'=> $this->asset->public
        ];
    }

    public function toVueDetailData(): array{
        return [
            'key'=> self::ASSET_KEY,
            'formComponent'=>'FormAssetDronePhoto',
            'viewComponent'=>'AssetViewDronePhoto',
            'collectionComponent'=>'AssetCollectionDronePhoto',
            'name'=>'Drone Photo',
            'namePlural'=>'Drone Photos'
        ];
    }
    
    public function newCollection(array $models = [])
    {
        return new AssetCollection($models);
    }
}
