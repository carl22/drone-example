<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCompany extends Model 
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'company_id',
    ];


    protected $casts = [
        'user_id'=> 'integer',
        'company_id'=>'integer',
    ];
}
