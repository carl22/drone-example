<?php

namespace App\Models;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Country extends Model 
{
    use HasFactory, SpatialTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'iso2',
        'iso3',
        'location'
    ];

    protected $spatialFields = [
        'location'
    ];


    public static function getIso2CodesAsCollection() {
        return self::pluck('iso2');
    }

    public static function findForIso2Collection(Collection $iso2Codes) {
        return self::query()->whereIn('iso2', $iso2Codes)->get();
    }
    
}
