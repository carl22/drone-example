<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyAttachedAsset extends Model 
{
    protected $table='asset_attached_companies';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'asset_id',
        'company_id',
    ];


    protected $casts = [
        'asset_id'=> 'integer',
        'company_id'=>'integer',
    ];
}
