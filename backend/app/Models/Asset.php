<?php

namespace App\Models;

use App\Facade\AssetFacade;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Asset extends Model 
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'public',
        'assetable_id',
        'assetable_type',
        'created_by_user_id',
        'mission_id'
    ];

    protected $casts = [
        'public'=>'boolean',
        'assetable_type'=>'string',
        'created_by_user_id'=>'integer',
        'mission_id'=>'integer'
    ];

    public function createdUser() {
        return $this->belongsTo(User::class, 'created_by_user_id','id');
    }

    public function mission() {
        return $this->belongsTo(Mission::class, 'mission_id','id');
    }

    public function getAssetDetailsAttribute(){
        $assetMap =  AssetFacade::getAssetRelationshipMap();
        if($assetMap->has($this->assetable_type)) {
            return $this->{$assetMap->get($this->assetable_type)};
        }
        return null;
    }
    public function scopeWithAssetDetails($query) {
        return $query->with(AssetFacade::getAssetRelationshipMap()->values()->toArray());
    }

}
