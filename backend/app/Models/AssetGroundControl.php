<?php

namespace App\Models;

use App\Collections\AssetCollection;
use App\Interfaces\Contracts\IAssetHandler;
use App\Interfaces\Services\IAssetService;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;

class AssetGroundControl extends Model implements IAssetHandler
{
    use HasFactory, ValidatesRequests, SpatialTrait;
    public const ASSET_KEY = 'ground_control';

    protected $table='ground_control_assets';
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'asset_id',
        'name',
        'description',
        'location'
    ];

    protected $spatialFields = [
        'location'
    ];


    public function asset() {
        return $this->belongsTo(Asset::class,'asset_id','id');
    }

    public function validateCreateRequest(Request $request, Asset $asset, IAssetService $service){
        $this->validate($request,[
            'name'=>'required|string|max:255',
            'description'=>'string|max:3000',
            'latitude'=>'required|numeric',
            'longitude'=>'required|numeric'
        ]);
    }
    public function validateUpdateRequest(Request $request, Asset $asset, IAssetService $service){
        $this->validate($request,[
            'name'=>'required|string|max:255',
            'description'=>'string|max:3000',
            'latitude'=>'required_with:longitude|numeric',
            'longitude'=>'required_with:latitude|numeric'
        ]);
    }
    public function handleCreate(Request $request, Asset $asset, IAssetService $service){
        $this->asset_id = $asset->id;
        $this->name = $request->get('name');
        $this->description = $request->get('description');
        $this->location = new Point($request->get('latitude'), $request->get('longitude'));
        $this->saveOrFail();
        return $this;
    }
    public function handleUpdate(Request $request, IAssetService $service){
        if($request->has('name')) {
            $this->name = $request->get('name');
        }
        if($request->has('description')) {
            $this->description = $request->get('description');
        }
        if($request->has('longitude') && $request->has('latitude')){
            $this->location = new Point($request->get('latitude'), $request->get('longitude'));
        }
        $this->saveOrFail();
        return $this;
    }
    public function handleDelete(Request $request, IAssetService $service){
        return $this->asset->delete();
    }
    
    public function toResourceAsArray(){
        return [
            'id'=>$this->asset_id,
            'name'=>$this->name,
            'description'=>$this->description,
            'longitude'=>$this->location->getLng(),
            'latitude'=>$this->location->getLat(),
            'public'=> $this->asset->public
        ];
    }
    
    public function toVueDetailData() : array{
        return [
            'key'=> self::ASSET_KEY,
            'formComponent'=>'FormAssetGroundControl',
            'viewComponent'=>'AssetViewGroundControl',
            'collectionComponent'=>'AssetCollectionGroundControl',
            'name'=>'Ground Control',
            'namePlural'=>'Ground Control'
        ];
    }

    public function newCollection(array $models = [])
    {
        return new AssetCollection($models);
    }

    
}
