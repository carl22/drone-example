<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mission extends Model 
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'company_id',
        'country_id',
        'public',
        'created_by_user_id'
    ];


    protected $casts = [
        'company_id'=>'integer',
        'country_id'=>'integer',
        'public'=>'boolean',
        'created_by_user_id'=>'integer'
    ];

    public function createdUser() {
        return $this->belongsTo(User::class, 'created_by_user_id','id');
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id','id');
    }

    public function company() {
        return $this->belongsTo(Company::class, 'company_id','id');
    }

}
