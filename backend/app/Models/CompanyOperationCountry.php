<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyOperationCountry extends Model 
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'country_id',
        'company_id',
    ];


    protected $casts = [
        'country_id'=> 'integer',
        'company_id'=>'integer',
    ];
}
