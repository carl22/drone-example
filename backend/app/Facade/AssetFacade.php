<?php
namespace App\Facade;

use App\Interfaces\Services\IAssetService;
use Illuminate\Support\Facades\Facade;

class AssetFacade extends Facade {
    protected static function getFacadeAccessor()
    {
        return IAssetService::class;
    }
}