<?php

namespace App\Providers;

use App\Interfaces\Services\IAssetService;
use App\Interfaces\Services\ICountryService;
use App\Interfaces\Services\IMissionService;
use App\Interfaces\Services\IUserAuthenticationService;
use App\Models\Asset;
use App\Models\AssetDronePhoto;
use App\Models\AssetGeoTagged;
use App\Models\AssetGroundControl;
use App\Services\AssetService;
use App\Services\CountryService;
use App\Services\MissionService;
use App\Services\UserAuthenticationService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(config_path('earthdrone.php'),'earthdrone');

        $this->app->bind(IUserAuthenticationService::class, UserAuthenticationService::class);
        $this->app->bind(ICountryService::class, CountryService::class);
        $this->app->bind(IMissionService::class, MissionService::class);
        $this->app->bind(IAssetService::class, AssetService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Asset::resolveRelationUsing('geoTaggedAsset', function ($assetModel) {
            return $assetModel->hasOne(AssetGeoTagged::class,'asset_id','id');
        });
        Asset::resolveRelationUsing('groundControlAsset', function ($assetModel) {
            return $assetModel->hasOne(AssetGroundControl::class,'asset_id','id');
        });
        Asset::resolveRelationUsing('dronePhotoAsset', function ($assetModel) {
            return $assetModel->hasOne(AssetDronePhoto::class,'asset_id','id');
        });
    }
}
