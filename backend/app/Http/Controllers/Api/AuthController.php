<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use App\Http\Traits\HandleApiExceptions;
use App\Interfaces\Services\IUserAuthenticationService;
use App\Models\Country;
use App\Services\UserAuthenticationService;
use Exception;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    use ValidatesRequests, HandleApiExceptions;
    protected $userAuthenticationService;

    public function __construct(IUserAuthenticationService $userAuthenticationService)
    {
        $this->userAuthenticationService = $userAuthenticationService;
    }

    public function login(Request $request) {

        $this->validate($request,[
            'email'=>'required|email|max:255',
            'password'=>'required|string|max:255',
            'remember'=>'boolean'
        ]);

        try {
            $user = $this->userAuthenticationService->authenticate($request->get('email'),$request->get('password'));

            Auth::login($user, $request->has('remember'));
            $request->session()->regenerate();
                
            return new UserResource($user);
        } catch(Exception $e) {
            return $this->handleExceptionResponse($e);
        }
    }

    public function register(Request $request) {

        $this->validate($request,[
            'name'=>'required|string|max:255',
            'email'=>'required|email|max:255|unique:users',
            'password'=>'required|string|max:255',
            'company_name'=>'required|string|max:255',
            'confirmed_certifications'=>'required|boolean',
            'consented'=>'required|boolean',
            'operational_countries'=>'required|array|min:1|max:255',
            'operational_countries.*'=>'required|in:'.Country::getIso2CodesAsCollection()->implode(',')
        ],
        [
            'confirmed_certifications.required'=>'Please Confirm you have all certifications.',
            'consented.required'=>'Please confirm you consent.'
        ]);

        try {
            DB::beginTransaction();
            $user = $this->userAuthenticationService->register(
                $request->get('name'),
                $request->get('email'),
                $request->get('password'),
                $request->get('company_name'),
                new Collection($request->get('operational_countries'))
            );

            Auth::login($user);
            $request->session()->regenerate();

            DB::commit();
            return new UserResource($user);
        }catch(Exception $e) {
            DB::rollBack();
            return $this->handleExceptionResponse($e);
        }
    }
}
