<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\MissionResource;
use App\Http\Traits\HandleApiExceptions;
use App\Interfaces\Services\ICountryService;
use App\Interfaces\Services\IMissionService;
use Exception;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MissionController extends Controller
{
    use HandleApiExceptions, ValidatesRequests;
    protected $missionService;
    protected $countryService;

    public function __construct(IMissionService $missionService, ICountryService $countryService)
    {
        $this->missionService = $missionService;
        $this->countryService = $countryService;
    }

    public function collection(Request $request) {
        try {
            $company = $request->user()->defaultCompany;
            return MissionResource::collection($this->missionService->getAllForCompany($company));
        }catch(Exception $e) {
            return $this->handleExceptionResponse($e);
        }
    }

    public function create(Request $request) {
        $this->validate($request,[
            'name'=>'required|string|max:255',
            'description'=>'string|max:3000',
            'public'=>'required|boolean',
            'country'=>'required|in:'.$this->countryService->getAll()->implode('iso2',',')
        ]);

        try {
           
            $mission = $this->missionService->factory();
            $mission->name = $request->get('name');
            $mission->description = $request->get('description');
            $mission->public = $request->get('public');
            $mission->created_by_user_id = $request->user()->id;
            $mission->country_id = $this->countryService->getByIso2($request->get('country'))->id;
            $mission->company_id = $request->user()->default_company_id;
            $mission = $this->missionService->createMission($mission);
            return new MissionResource($mission);
        }catch(Exception $e) {
            return $this->handleExceptionResponse($e);
        }    
    }

    public function update(Request $request, $missionId) {
        $this->validate($request,[
            'name'=>'string|max:255',
            'description'=>'string|max:3000',
            'public'=>'boolean',
            'country'=>'in:'.$this->countryService->getAll()->implode('iso2',',')
        ]);

        try {
        
            $company = $request->user()->defaultCompany;

            $mission = $this->missionService->findByIdAndCompany($company, $missionId);
            if($request->has('name')) {
                $mission->name = $request->get('name');
            }
            if($request->has('description')) {
                $mission->description = $request->get('description');
            }
            if($request->has('public')) {
                $mission->public = $request->get('public');
            }
            if($request->has('country')) {
                $mission->country_id = $this->countryService->getByIso2($request->get('country'))->id;
            }
    
            $mission = $this->missionService->createMission($mission);
            return new MissionResource($mission);
        }catch(Exception $e) {
            return $this->handleExceptionResponse($e);
        } 
    }

    public function delete(Request $request, $missionId) {
        try {
            $company = $request->user()->defaultCompany;
            $mission = $this->missionService->findByIdAndCompany($company, $missionId);
            return [
                'data'=> [
                    'success'=>$this->missionService->deleteMission($mission)
                ]
            ];
        }catch(Exception $e) {
            return $this->handleExceptionResponse($e);
        } 
    }

    public function getOne(Request $request, $missionId) {
        try {
            $company = $request->user()->defaultCompany;
            $mission = $this->missionService->findByIdAndCompany($company, $missionId);
            return new MissionResource($mission);
        }catch(Exception $e) {
            return $this->handleExceptionResponse($e);
        } 
    }
   
}
