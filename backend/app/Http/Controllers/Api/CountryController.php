<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CountryResource;
use App\Http\Traits\HandleApiExceptions;
use App\Interfaces\Services\ICountryService;
use App\Models\Country;
use Exception;
use Illuminate\Routing\Controller;

class CountryController extends Controller
{
    use HandleApiExceptions;
    protected $countryService;

    public function __construct(ICountryService $countryService)
    {
        $this->countryService = $countryService;
    }

    public function collection() {
        try {
            return CountryResource::collection($this->countryService->getAll());
        } catch(Exception $e) {
            return $this->handleExceptionResponse($e);
        }
    
    }
   
}
