<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\InvalidArgumentException;
use App\Http\Resources\AssetResource;
use App\Http\Resources\AssetTypeResource;
use App\Http\Traits\HandleApiExceptions;
use App\Interfaces\Services\IAssetService;
use App\Interfaces\Services\IMissionService;
use App\Models\Asset;
use App\Services\AssetService;
use Exception;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class AssetController extends Controller
{
    use HandleApiExceptions, ValidatesRequests;
    protected $assetService;
    protected $missionService;

    public function __construct(IAssetService $assetService, IMissionService $missionService)
    {
        $this->assetService = $assetService;
        $this->missionService = $missionService;
    }

    public function collection(Request $request, $missionId, $type) {
        try {
            $company = $request->user()->defaultCompany;
            $mission = $company->missions()->findOrFail($missionId);
            $assetType = $this->assetService->assetFactory($type);
            if(is_null($assetType)) {
                throw new InvalidArgumentException('type does not exist');
            }
            $typeClass = get_class($assetType);

            $assets = Asset::query()
            ->where('mission_id', '=', $mission->id)
            ->where('assetable_type', '=', $typeClass)
            ->withAssetDetails()
            ->get();

            $assetTypes = $assets->map(function(Asset $asset){ return $asset->getAssetDetailsAttribute(); });

            return AssetResource::collection($assetTypes);

        }catch(Exception $e) {
            return $this->handleExceptionResponse($e);
        }
    } 

    public function typeCollection() {
        try {
            return [
                'data'=>$this->assetService->getVueDataCollection()->values()->toArray()
            ];
        }catch(Exception $e) {
            return $this->handleExceptionResponse($e);
        }
    }
   
    public function create(Request $request, $type) {
        $this->validate($request, [
            'public'=>'required|boolean',
            'mission_id'=>'required|in:'.$request->user()->defaultCompany->missions()->pluck('id')->implode(',')
        ]);
     
        try {
            $assetType = $this->assetService->assetFactory($type);
            if(is_null($assetType)) {
                throw new InvalidArgumentException('type does not exist');
            }

            DB::beginTransaction();

            $company = $request->user()->defaultCompany;
            $mission = $this->missionService->findByIdAndCompany($company, $request->get('mission_id'));

            $asset = new Asset();
            $asset->public = $request->get('public');
            $asset->mission_id = $mission->id;
            $asset->created_by_user_id = $request->user()->id;
            $asset->assetable_type = get_class($assetType);
            $asset->saveOrFail();

            $assetType->validateCreateRequest($request, $asset, $this->assetService);
            $savedAsset = $assetType->handleCreate($request, $asset, $this->assetService);
            
            DB::commit();

            return new AssetResource($savedAsset);

        }catch(ValidationException $e) {
            DB::rollBack();
            throw $e;
        } catch(Exception $e) {
            DB::rollBack();
            return $this->handleExceptionResponse($e);
        }
    }

    public function update(Request $request, $assetId) {
        $this->validate($request, [
            'public'=>'boolean',
            'mission_id'=>'in:'.$request->user()->defaultCompany->missions()->pluck('id')->implode(',')
        ]);

        try {
            DB::beginTransaction();
            $company = $request->user()->defaultCompany;
            $asset = $this->assetService->findByIdAndCompany($company, $assetId);
            if(is_null($asset)) {
                throw new InvalidArgumentException('Asset was not found');
            }
            if($request->has('public')) {
                $asset->public = $request->get('public');
            }
            if($request->has('mission_id')) {
                $asset->mission_id = $request->get('mission_id');
            }
            $asset->saveOrFail();

            $asset->assetDetails->validateUpdateRequest($request, $asset, $this->assetService);
            $savedAsset = $asset->assetDetails->handleUpdate($request, $this->assetService);
            
            DB::commit();

            return new AssetResource($savedAsset);

        }catch(ValidationException $e) {
            DB::rollBack();
            throw $e;
        }catch(Exception $e) {
            DB::rollBack();
            return $this->handleExceptionResponse($e);
        }
       
    }

    public function delete(Request $request, $assetId) {

        try {
            DB::beginTransaction();
            $company = $request->user()->defaultCompany;
            $asset = $this->assetService->findByIdAndCompany($company, $assetId);
            if(is_null($asset)) {
                throw new InvalidArgumentException('Asset was not found');
            }

            $deletedAsset = $asset->assetDetails->handleDelete($request, $this->assetService);
            
            DB::commit();

            return [
                'data'=>[
                    'success'=>$deletedAsset
                ]
            ];
        }catch(Exception $e) {
            DB::rollBack();
            return $this->handleExceptionResponse($e);
        }
       
    }
}
