<?php
namespace App\Http\Traits;

use App\Exceptions\BaseApiException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;

trait HandleApiExceptions {
    protected function handleExceptionResponse(Exception $e) {
        if($e instanceof BaseApiException) {
            return new JsonResponse($e->toArray(), $e->getStatusCode());
        }

        Log::error($e);

        return new JsonResponse(
            [
                'error'=>'Unknown Error',
                'code'=>500
            ],
        500);
    }
}