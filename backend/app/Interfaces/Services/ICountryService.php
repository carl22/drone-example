<?php
namespace App\Interfaces\Services;

use App\Interfaces\Contracts\IAuthenticatableToken;
use App\Models\Country;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;

interface ICountryService {
    public function getAll() : EloquentCollection;
    public function getByIso2(string $iso2): Country;
}