<?php
namespace App\Interfaces\Services;

use App\Interfaces\Contracts\IAssetHandler;
use App\Models\Asset;
use App\Models\Company;
use Illuminate\Support\Collection;

interface IAssetService {
    public function getAssetRelationshipMap():Collection;
    public function getAssetKeys() :Collection;
    public function getVueDataCollection() : Collection;
    public function assetFactory(string $type) : IAssetHandler | null;
    public function getStorageDriver() : string ;
    public function findByIdAndCompany(Company $company, $assetId): Asset;
}