<?php
namespace App\Interfaces\Services;

use App\Interfaces\Contracts\IAuthenticatableToken;
use Illuminate\Support\Collection;

interface IUserAuthenticationService {
    public function authenticate(string $email, string $password) : IAuthenticatableToken;
    public function register(string $name, string $email, string $password, string $companyName, Collection $operationalIso2Countries ) : IAuthenticatableToken;
}