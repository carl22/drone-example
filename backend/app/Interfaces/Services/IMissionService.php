<?php
namespace App\Interfaces\Services;

use App\Models\Company;
use App\Models\Mission;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;

interface IMissionService {
    public function getAllForCompany(Company $company) : EloquentCollection;
    public function factory(): Mission;
    public function findByIdAndCompany(Company $company, $missionId) : Mission;
    public function createMission(Mission $mission) : Mission;
    public function updateMission(Mission $mission) : Mission;
    public function deleteMission(Mission $mission) : bool;
}