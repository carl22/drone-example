<?php
namespace App\Interfaces\Contracts;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Support\Arrayable;

interface IAuthenticatableToken extends Authenticatable, Arrayable {
    public function tokens();
    public function tokenCan(string $ability);
    public function createToken(string $name, array $abilities = ['*']);
    public function currentAccessToken();
    public function withAccessToken($accessToken);
}