<?php
namespace App\Interfaces\Contracts;

use App\Interfaces\Services\IAssetService;
use App\Models\Asset;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;

interface IAssetHandler extends Arrayable {
    public function validateCreateRequest(Request $request, Asset $asset, IAssetService $service);
    public function validateUpdateRequest(Request $request, Asset $asset, IAssetService $service);
    public function handleCreate(Request $request, Asset $asset, IAssetService $service);
    public function handleUpdate(Request $request, IAssetService $service);
    public function handleDelete(Request $request, IAssetService $service);
    public function toResourceAsArray();
    public function toVueDetailData() : array;
}