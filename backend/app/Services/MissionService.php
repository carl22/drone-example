<?php
namespace App\Services;

use App\Exceptions\InvalidArgumentException;
use App\Interfaces\Services\IMissionService;
use App\Models\Company;
use App\Models\Mission;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class MissionService implements IMissionService {
    public function getAllForCompany(Company $company): Collection
    {
        return Mission::query()
            ->where('company_id', '=', $company->id)
            ->orderBy('created_at')
            ->get();
    }

    public function factory(): Mission
    {
        return new Mission();
    }

    public function findByIdAndCompany(Company $company, $missionId) : Mission {
        return Mission::query()
            ->where('company_id','=', $company->id)
            ->where('id','=', $missionId)
            ->firstOrFail();
    }

    public function createMission(Mission $mission) : Mission {
        $this->validateMission($mission);
        $mission->saveOrFail();
        return $mission;
    }

    public function updateMission(Mission $mission) : Mission{
        $this->validateMission($mission);
        $mission->saveOrFail();
        return $mission;
    }

    public function deleteMission(Mission $mission) : bool{
        return $mission->delete();
    }

    protected function validateMission(Mission $mission) {
        if(is_null($mission->company)) {
            throw new InvalidArgumentException('Company is not set');
        }
        if(is_null($mission->createdUser)) {
            throw new InvalidArgumentException('User is not set');
        }
        if($mission->createdUser->isInCompany($mission->company) === false) {
            throw new InvalidArgumentException('User is not part of company');
        }
        if(is_null($mission->country)) {
            throw new InvalidArgumentException('country is not set');
        }
    }
}