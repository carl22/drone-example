<?php
namespace App\Services;

use App\Interfaces\Contracts\IAuthenticatableToken;
use App\Interfaces\Services\IUserAuthenticationService;
use App\Models\Company;
use App\Models\CompanyOperationCountry;
use App\Models\Country;
use App\Models\User;
use App\Models\UserCompany;
use Carbon\Carbon;
use App\Exceptions\AuthenticationException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\InvalidArgumentException;

class UserAuthenticationService implements IUserAuthenticationService {

    public function authenticate(string $email, string $password) : IAuthenticatableToken {
        $user = User::query()->where('email','=',$email)->first();
        if($user === null || Hash::check($password, $user->password) === false) {
            throw new AuthenticationException('Email invalid or password incorrect');
        }
        return $user;
    }

    public function register(string $name, string $email, string $password, string $companyName, Collection $operationalIso2Countries ) : IAuthenticatableToken {
        $operationalCountries = Country::findForIso2Collection($operationalIso2Countries->unique());
        
        if($operationalCountries->count() === 0) {
            throw new InvalidArgumentException('At least 1 operational country is required');
        }

        $company = new Company();
        $company->name = $companyName;
        $company->saveOrFail();

        $insertOpertionalCountry = $operationalCountries->map(function(Country $country) use($company) {
            return [
                'country_id'=>$country->id,
                'company_id'=>$company->id
            ];
        });

        CompanyOperationCountry::insert($insertOpertionalCountry->toArray());

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->default_company_id = $company->id;
        $user->saveOrFail();

        $company->consented_at = Carbon::now();
        $company->consented_by = $user->id;
        $company->confirmed_certifications_at = Carbon::now();
        $company->confirmed_certifications_by = $user->id;
        $company->saveOrFail();

        $userCompany = new UserCompany();
        $userCompany->user_id = $user->id;
        $userCompany->company_id = $company->id;
        $userCompany->saveOrFail();

        return $user;

    }
}