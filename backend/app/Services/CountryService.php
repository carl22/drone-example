<?php
namespace App\Services;

use App\Interfaces\Services\ICountryService;
use App\Models\Country;
use Illuminate\Database\Eloquent\Collection;

class CountryService implements ICountryService {
    public function getAll() : Collection {
        return Country::query()->orderBy('name')->get();
    }
    
    public function getByIso2(string $iso2) : Country{
        return Country::query()->where('iso2','=',$iso2)->first();
    }
}