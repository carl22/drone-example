<?php
namespace App\Services;

use App\Interfaces\Contracts\IAssetHandler;
use App\Interfaces\Services\IAssetService;
use App\Models\Asset;
use App\Models\Company;
use Illuminate\Support\Collection;

class AssetService implements IAssetService {
    
    public function getAssetRelationshipMap():Collection  {
        return new Collection(config('earthdrone.assetTypes'));
    }

    public function getStorageDriver() : string {
        return config('earthdrone.storageDriver');
    }

    public function getAssetKeys() :Collection {
        return $this->getAssetRelationshipMap()->mapWithKeys(function($relationNAme, $className){
            return [$className=>$className::ASSET_KEY];
        });
    }

    public function getVueDataCollection() : Collection {
        return $this->getAssetRelationshipMap()->map(function($relationName, $className) {
            return (new $className)->toVueDetailData();
        });
    }

    public function assetFactory(string $type) : IAssetHandler | null {
        $keys = $this->getAssetKeys()->flip();
        if($keys->has($type)) {
            $className = $keys->get($type);
            return new $className;
        }
        return null;
    }

    public function findByIdAndCompany(Company $company, $assetId): Asset
    {
        return Asset::query()
            ->whereHas('mission', function($query) use($company) {
                return $query->where('company_id', '=', $company->id);
            })
            ->where('id','=', $assetId)
            ->first();
    }
}