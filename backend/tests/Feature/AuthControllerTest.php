<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test a Successful Login
     *
     * @return void
     */
    public function test_login_successful()
    {
        $user = User::factory()->create();
        $response = $this->postJson('/api/login',['email'=>$user->email,'password'=>'password']);

        $response->assertStatus(200);
        $response->assertJsonFragment(['id'=>$user->id,'email'=>$user->email]);
    }

     /**
     * Test a Invalid Password
     *
     * @return void
     */
    public function test_login_invalid_password()
    {
        $user = User::factory()->create();
        $response = $this->postJson('/api/login',['email'=>$user->email,'password'=>'invalid']);

        $response->assertStatus(403);
        $response->assertJsonFragment(['code'=>403]);
    }

     /**
     * Test a Invalid Email
     *
     * @return void
     */
    public function test_login_invalid_email()
    {
        $user = User::factory()->create();
        $response = $this->postJson('/api/login',['email'=>'invalid'.$user->email,'password'=>'invalid']);

        $response->assertStatus(403);
        $response->assertJsonFragment(['code'=>403]);
    }

    /**
     * Test a Invalid Email
     *
     * @return void
     */
    public function test_login_no_email()
    {
        $response = $this->postJson('/api/login',['password'=>'invalid']);

        $response->assertStatus(422);
        $response->assertJsonFragment([
            'errors'=>[
                'email'=>['The email field is required.']
            ]]);
    }

    /**
     * Test a Invalid Email
     *
     * @return void
     */
    public function test_login_invalid_email_entry()
    {
        $response = $this->postJson('/api/login',['email'=>'invalidemail','password'=>'invalid']);

        $response->assertStatus(422);
        $response->assertJsonFragment([
            'errors'=>[
                'email'=>['The email must be a valid email address.']
            ]]);
    }


        /**
     * Test a Invalid Email
     *
     * @return void
     */
    public function test_login_no_password()
    {
        $user = User::factory()->create();
        $response = $this->postJson('/api/login',['email'=>'invalid'.$user->email]);

        $response->assertStatus(422);
        $response->assertJsonFragment([
            'errors'=>[
                'password'=>['The password field is required.']
            ]]);
    }

         /**
     * Test a Invalid Email
     *
     * @return void
     */
    public function test_register_confirm_consents()
    {
        $user = User::factory()->create();
        $response = $this->postJson('/api/register',['email'=>'test@test.com']);

        $response->assertStatus(422);
        $response->assertJsonValidationErrors([
            'consented'=>['Please confirm you consent.'],
            'confirmed_certifications'=>['Please Confirm you have all certifications.']
        ]);
    }

    
}
