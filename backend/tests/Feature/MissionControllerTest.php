<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\Country;
use App\Models\Mission;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MissionControllerTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_cannot_get_missions() {
        [$mission, $user] = $this->createMission();

        $response = $this->getJson('/api/mission');

        $response->assertStatus(401);
    
    }


    public function test_can_get_missions() {
        [$mission, $user] = $this->createMission();

        if($user instanceof Authenticatable) {
            $this->actingAs($user);
        }
    
        $response = $this->getJson('/api/mission');

        $response->assertStatus(200);
        $response->assertJsonFragment(['data'=>[[
            'id'=>$mission->id,
            'name'=>$mission->name,
            'description'=>$mission->description,
            'country'=>$mission->country->iso2,
            'public'=>$mission->public
        ]]]);
    }

    public function test_can_create_mission(){
        [$mission, $user,$country] = $this->createMission();

        if($user instanceof Authenticatable) {
            $this->actingAs($user);
        }
        $missionData = [
            'name'=>'Test Mission',
            'description'=>'my description',
            'public'=>true,
            'country'=>$country->iso2
        ];
    
        $response = $this->postJson('/api/mission',$missionData);

        $response->assertStatus(201);


        $response->assertJsonPath('data.name',$missionData['name']);
        $response->assertJsonPath('data.description',$missionData['description']);
        $response->assertJsonPath('data.public',$missionData['public']);
        $response->assertJsonPath('data.country',$missionData['country']);
    }

    public function test_can_update_mission(){
        [$mission, $user,$country] = $this->createMission();

        if($user instanceof Authenticatable) {
            $this->actingAs($user);
        }
        $missionData = ['name'=>'updated mission'];
    
        $response = $this->putJson('/api/mission/'.$mission->id,$missionData);

        $response->assertStatus(200);


        $response->assertJsonPath('data.name','updated mission');
    }

    public function test_can_delete_mission(){
        [$mission, $user,$country] = $this->createMission();

        if($user instanceof Authenticatable) {
            $this->actingAs($user);
        }
    
    
        $response = $this->deleteJson('/api/mission/'.$mission->id);

        $response->assertStatus(200);

        $response->assertJsonPath('data.success',true);
    }

    protected function createMission() {
        $country = Country::factory()->create();
        $user = User::factory()->create();
        $company = Company::factory()->create();
        $user->default_company_id = $company->id;
        $user->save();
        $mission = Mission::factory()->create([
            'company_id'=>$company->id,
            'country_id'=>$country->id,
            'created_by_user_id'=>$user->id
        ]);

        return [$mission, $user, $country];
    }
    
}
