<?php

namespace Tests\Feature;

use App\Models\Country;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CountryControllerTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_can_get_countries() {
        $country = Country::factory()->create();

        $response = $this->get('/api/country');

        $response->assertJsonFragment([[
            'id'=>$country->iso2,
            'name'=>$country->name
        ]]);
    }

    
}
