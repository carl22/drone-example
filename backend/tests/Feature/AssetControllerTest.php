<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\Country;
use App\Models\Mission;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AssetControllerTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_can_get_types() {
        $user = User::factory()->create();
        if($user instanceof Authenticatable) {
            $this->actingAs($user);
        }

        $response = $this->get('/api/asset/$types');

        $response->assertStatus(200);
    }

    public function test_can_create_drone_photo() {
        [$mission, $user, $country] = $this->createMission();
        if($user instanceof Authenticatable) {
            $this->actingAs($user);
        }

        Storage::fake('public');

        $file = UploadedFile::fake()->image('avatar.jpg');

        $response = $this->post('/api/asset/drone_photo',[
            'mission_id'=>$mission->id,
            'public'=>true,
            'image'=>$file,
            'name'=>'test image'
        ]);

        $response->assertStatus(201);
        $response->assertJsonPath('data.name','test image');
    }
    

    protected function createMission() {
        $country = Country::factory()->create();
        $user = User::factory()->create();
        $company = Company::factory()->create();
        $user->default_company_id = $company->id;
        $user->save();
        $mission = Mission::factory()->create([
            'company_id'=>$company->id,
            'country_id'=>$country->id,
            'created_by_user_id'=>$user->id
        ]);

        return [$mission, $user, $country];
    }
    
}
