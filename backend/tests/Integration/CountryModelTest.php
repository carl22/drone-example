<?php

namespace Tests\Integration;

use App\Interfaces\Contracts\IAuthenticatableToken;
use App\Interfaces\Services\IUserAuthenticationService;
use App\Models\Country;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Tests\TestCase;

use function PHPUnit\Framework\assertTrue;

class CountryModelTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test can get iso codes 
     * 
     * @return void
     */
    public function test_can_get_iso_codes()
    {
        $country = Country::factory()->create();

        $isoCodes = Country::getIso2CodesAsCollection();

        $this->assertInstanceOf(Collection::class, $isoCodes);
        $this->assertCount(1, $isoCodes);
        $this->assertEquals($country->iso2, $isoCodes->first());
    }

}
