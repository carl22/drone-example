<?php

namespace Tests\Integration;

use App\Exceptions\AuthenticationException;
use App\Interfaces\Contracts\IAuthenticatableToken;
use App\Interfaces\Services\IUserAuthenticationService;
use App\Models\Country;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

use function PHPUnit\Framework\assertEquals;

class UserAuthenticationServiceTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test can authenticate a user. 
     * 
     * @return void
     */
    public function test_can_authenticate_user()
    {
        $emailAddress = 'carl@coxeh.com';
        $password = 'password';
        $user = User::factory()->create(['email'=>$emailAddress]);
        $authenticationService = $this->app->make(IUserAuthenticationService::class);

        $authedUser = $authenticationService->authenticate($emailAddress, $password);

        $this->assertInstanceOf(IAuthenticatableToken::class, $authedUser,'User Could not be authenticated');
        $this->assertEquals($user->id, $authedUser->id, 'Wrong user returned');
    }

     /**
     * Test throws an exception on invalid password
     * 
     * @return void
     */
    public function test_invalid_password_throws_exception()
    {
        $emailAddress = 'carl@coxeh.com';
        $password = 'not my password';
        $user = User::factory()->create(['email'=>$emailAddress]);
        $authenticationService = $this->app->make(IUserAuthenticationService::class);

        $this->expectException(AuthenticationException::class);

        $authenticationService->authenticate($emailAddress, $password);

    }

    /**
     * Test throws an exception on invalid email
     * 
     * @return void
     */
    public function test_invalid_email_throws_exception()
    {
        $emailAddress = 'carl@coxeh.com';
        $password = 'not my password';
        $user = User::factory()->create(['email'=>'nomy@email.com']);
        $authenticationService = $this->app->make(IUserAuthenticationService::class);

        $this->expectException(AuthenticationException::class);

        $authenticationService->authenticate($emailAddress, $password);

    }

    public function test_successful_registration() {
        $name = 'Carl';
        $email = 'carl@coxeh.com';
        $password = 'password';
        $compayName = 'Coxeh';

        $country = Country::factory()->create();

        $authenticationService = $this->app->make(IUserAuthenticationService::class);
        
        $createdUser = $authenticationService->register($name, $email, $password, $compayName, new Collection([$country->iso2]));

        $this->assertInstanceOf(User::class, $createdUser);
        $this->assertEquals($name, $createdUser->name);
        $this->assertEquals($email, $createdUser->email);
        $this->assertTrue(Hash::check($password, $createdUser->password));
        $this->assertEquals($compayName, $createdUser->defaultCompany->name);
        $this->assertCount(1, $createdUser->companies);
        $this->assertCount(1, $createdUser->defaultCompany->operationCountries);
        $this->assertEquals($createdUser->id, $createdUser->defaultCompany->consentedUser->id);
        $this->assertEquals($createdUser->id, $createdUser->defaultCompany->confirmedCertificationUser->id);
    }

}
