<?php

namespace Tests\Unit;

use App\Facade\AssetFacade;
use App\Interfaces\Services\IAssetService;
use Illuminate\Support\Collection;
use Tests\TestCase;

use function PHPUnit\Framework\assertInstanceOf;

class AssetServiceTest extends TestCase
{
    public function test_can_get_relationship_map()
    {
        $assetService = $this->app->make(IAssetService::class);

        $assetMap = $assetService->getAssetRelationshipMap();
        
        $this->assertInstanceOf(Collection::class, $assetMap);
    }

    public function test_can_get_asset_keys()
    {
        $assetService = $this->app->make(IAssetService::class);

        $assetKeys = $assetService->getAssetKeys();
        
        $this->assertInstanceOf(Collection::class, $assetKeys);
    }

    public function test_facade_is_mapped_to_service() {
        $assetFacade = AssetFacade::getFacadeRoot();

        $this->assertInstanceOf(IAssetService::class, $assetFacade);
    }
}
