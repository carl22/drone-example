<?php

use App\Http\Controllers\Api\AssetController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CountryController;
use App\Http\Controllers\Api\MissionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', AuthController::class.'@login');
Route::post('register', AuthController::class.'@register');

Route::get('country',CountryController::class.'@collection');

Route::middleware('auth:sanctum')->group(function() {
    
    Route::get('/mission', MissionController::class.'@collection');
    Route::post('/mission', MissionController::class.'@create');
    Route::get('/mission/{missionId}', MissionController::class.'@getOne');
    Route::put('/mission/{missionId}', MissionController::class.'@update');
    Route::delete('/mission/{missionId}', MissionController::class.'@delete');

    Route::get('/mission/{missionId}/asset/{type}',AssetController::class.'@collection');
    
    Route::get('/asset/$types',AssetController::class.'@typeCollection');
    Route::post('/asset/{type}', AssetController::class.'@create');
    Route::post('/asset/{assetId}/update', AssetController::class.'@update');
    Route::delete('/asset/{assetId}', AssetController::class.'@delete');

});
