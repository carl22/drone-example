<?php

use App\Models\AssetDronePhoto;
use App\Models\AssetGeoTagged;
use App\Models\AssetGroundControl;

return [
    'assetTypes' => [
        AssetGeoTagged::class => 'geoTaggedAsset',
        AssetGroundControl::class => 'groundControlAsset',
        AssetDronePhoto::class=>'dronePhotoAsset'
    ],
    'storageDriver'=>'public'
];