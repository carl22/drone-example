<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'consented_at' => $this->faker->dateTime(),
            'confirmed_certifications_at' => $this->faker->dateTime(),
            'consented_by'=>null,
            'confirmed_certifications_by'=>null
        ];
    }
}
