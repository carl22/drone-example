<?php

namespace Database\Factories;

use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'iso2' => $this->faker->countryCode(),
            'iso3' => $this->faker->currencyCode(),
            'location'=> new Point($this->faker->latitude(), $this->faker->longitude())
        ];
    }
}
