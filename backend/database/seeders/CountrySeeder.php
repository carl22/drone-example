<?php

namespace Database\Seeders;

use App\Models\Country;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    protected $countries = [
        [
            'name' =>'United Kingdom',
            'iso3' => 'GBR',
            'iso2' => 'GB',
            'latitude' => 55.3781,
            'longitude' => 3.4360
        ],
        [
            'name' =>'Australia',
            'iso3' => 'AUS',
            'iso2' => 'AU',
            'latitude' => 133.7751,
            'longitude' => 25.2744
        ],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->countries as $country) {
            $model = new Country();
            $model->name = $country['name'];
            $model->iso3 = $country['iso3'];
            $model->iso2 = $country['iso2'];
            $model->location = new Point($country['latitude'], $country['longitude']);
            $model->save();
        }
    }
}
