<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->dateTime('consented_at')->nullable();
            $table->unsignedBigInteger('consented_by')->nullable();
            $table->dateTime('confirmed_certifications_at')->nullable();
            $table->unsignedBigInteger('confirmed_certifications_by')->nullable();
            $table->foreign('consented_by')->references('id')->on('users');
            $table->foreign('confirmed_certifications_by')->references('id')->on('users');
        });

        Schema::create('user_companies', function(Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('company_id');
            $table->foreign('user_id','uc_user_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('company_id','uc_company_id')->references('id')->on('companies')->onDelete('CASCADE');
        });

        Schema::create('countries', function(Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name');
            $table->char('iso2', 2);
            $table->char('iso3', 3);
            $table->point('location');
        });

        Schema::create('company_operation_countries', function(Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('country_id');
            $table->foreign('country_id','coc_country_id')->references('id')->on('countries')->onDelete('CASCADE');
            $table->foreign('company_id','coc_company_id')->references('id')->on('companies')->onDelete('CASCADE');
        });

        Schema::table('users', function(Blueprint $table) {
            $table->unsignedBigInteger('default_company_id')->nullable();
            $table->foreign('default_company_id','users_default_company')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropForeign('users_default_company');
            $table->dropColumn(['default_company_id']);
        });
        Schema::dropIfExists('company_operation_countries');
        Schema::dropIfExists('countries');
        Schema::dropIfExists('user_companies');
        Schema::dropIfExists('companies');
    }
}
