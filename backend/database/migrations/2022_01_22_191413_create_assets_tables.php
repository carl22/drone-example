<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('mission_id');
            $table->unsignedBigInteger('created_by_user_id');
            $table->boolean('public');
            $table->string('assetable_type');
            $table->foreign('mission_id')->references('id')->on('missions')->onDelete('CASCADE');
            $table->foreign('created_by_user_id')->references('id')->on('users');
        });

        Schema::create('asset_attached_companies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('mission_id');
            $table->foreign('mission_id')->references('id')->on('missions')->onDelete('CASCADE');
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::create('geotag_photo_assets', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('asset_id');
            $table->string('name');
            $table->string('drive');
            $table->string('path');
            $table->string('type');
            $table->point('location');
            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('CASCADE');
        });

        Schema::create('drone_photo_assets', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('asset_id');
            $table->string('name');
            $table->string('drive');
            $table->string('path');
            $table->string('type');
            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('CASCADE');
        });

        Schema::create('ground_control_assets', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('asset_id');
            $table->string('name');
            $table->text('description');
            $table->point('location');
            $table->foreign('asset_id')->references('id')->on('assets')->onDelete('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ground_control_assets');
        Schema::dropIfExists('drone_photo_assets');
        Schema::dropIfExists('geotag_photo_assets');
        Schema::dropIfExists('asset_attached_companies');
        Schema::dropIfExists('assets');
    }
}
