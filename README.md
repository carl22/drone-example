# Earth Drone
An application to manage drone activity. Features include:

* User/Supplier Registration and Login
* Can Select Opertional Countries on Registration and Mission Forms
* Registation must consent to data being shared and that suppliers have correct certifications
* CSRF Protection
* CORS
* Create Multiple Missions
* Select/Choose Data for each Mission  (currently only; Geo Photos, Drone Photos & Ground Control Setup)
* Data written as a module/plugin for Vue and PHP to allow ease of "data" object to be added
* Image Uploads
* POC of Technologies (PHP, Vue, Vuex, Bootstrap, Sass, MySQL, Typescript, Docker)
* Responsive Design for Mobile
* Docker building containers to be uploaded to repositories where ENV variables used to deploy anywhere.
* Unit, Feature, Integration and End to End Test Examples

## Setup Environment
Please make sure you have [docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/) installed. These are the only dependency needed to run this application.

Tested on:

 * Linux
 * Mac

## Running the Example
Running the example is as easy as running one command below. This will start by building production ready docker containers, and add environment variables automatically to allow you to run on any platform.

    npm run example    

First time you start the example it will take a while to build the containers. Once done and MySQL is up and running you will need to run the command below in another terminal while the containers are running. This will perform a couple of setup commands such as setting up database and seeding base data of the database.

    npm run init

You will know when the container are running as the mysql container will display something like this in terminal (ready for connections is the important part):

    mysql_1     | 2022-01-24T19:55:29.856360Z 0 [System] [MY-010931] [Server] /usr/sbin/mysqld: ready for connections. Version: '8.0.28'  socket: '/var/lib/mysql/mysql.sock'  port: 3306  MySQL Community Server - GPL.

Visit the frontend on http://localhost:3000

if any files are changed then run `npm run build:example`. This will recompile the containers so you are able to run them again. Alternativly run in development mode.

## Development Setup
To run a development environement please run the command below. This will run the required containers and setup volumes to the relevent local directories to allow you to edit files.

    npm run dev

Once Running You will need to create and seed the database. While the containers are running in a seperate terminal run

    npm run init    

Visit the frontend on http://localhost:3000

Composer and artisan can be run via the container or use these utility methods

    npm run artisan {command}
    npm run composer {command}

## Tests
Test are run in the same container as development. These are run seperatly using these commands:

    npm run test:frontend
    npm run test:backend
    
End to End Testing install cypress then run the test.

    npm install
    npm run test:cypress

Note: backend and cypress will require the example/dev containers to be running before they will work.    
Note: running the test backend will wipe the database as those are done on the same database. This is to keep things simple for this example.

## Todo / Wishlist

* Setup CI using gitlab/github/bitbucket pipelines
* Setup auth mechanism to use OAuth2/JWT tokens instead of cookie based sessions.
* Postman Collection to be added to documentation to Show developers how to interact with API
* Setup On a Live Server
* Data and missions to be shared when set to public allowing other companies to search and select data
* Fronend to impliment search of public data. geo data to be searchable via distance
* Fronend geo data to display via google maps or simular
* Companies can be connected to multiple users. However this is currently limited to the registrant. 
* Email verification on registation
* Forgot password mechanism
* Logo and custom CSS theme
* Caching of Data from the database
* Caching of data in Vuex stores more effiently
* Implement Python/Django backend to replace PHP
* make money :)

## Database Schema

![Database Schema](./databaseSchema.png "Database schema")