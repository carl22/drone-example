export default {
    props: {
        default: {
            type: Object,
            default() {
                return {}
            }
        }
    },
    data() {
        return {
            formData: {}
        }
    },
    created() {
        this.resetForm()
    },
    watch: {
        formData(newData) {
            this.$emit('input', newData)
        }
    },
    methods: {
        resetForm() {
            this.formData = {}
            const defaultKeys = Object.keys(this.default);
            defaultKeys.forEach(k => this.$set(this.formData, k, this.default[k]))
            return this
        },
        onSubmit() {
            if(this.$v) {
                this.$v.$touch();
            }
            if(!this.$v || this.$v.$invalid === false) {
                let formData = this.formData;
                if (typeof this.onSubmitHandler==='function') {
                    formData = this.onSubmitHandler(formData)
                }
                this.$emit('submit', formData, this)
            }  
        }
    }
}