export const formDataBind = (key) => {
    return {
        get() {
            return this.formData[key]
        },
        set(value) {
            this.$set(this.formData, key, value)
        }
    }
}