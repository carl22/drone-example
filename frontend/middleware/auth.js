export default function ({ store, redirect }) {
    if (!store.getters['user/loggedIn']) {
      return redirect('/')
    }
  }
  