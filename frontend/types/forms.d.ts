export interface ILoginData {
    email: string,
    password: string,
    remember: boolean
  }
  
  export interface IRegisterData {
    name: string,
    email: string,
    password: string,
    password_confirm: string
    company_name: string,
    confirmed_certifications: boolean,
    consented: boolean,
    operational_countries: Array<string>
  }

  export interface IMissionData {
    id?: number,
    name: string,
    description: string,
    public: boolean,
    country: string
  }