export interface ILoginUserResponse {
    id: number
    email: string
    name: string,
    company: string
}

export interface ICountryResponse {
    id: string,
    name: string
}

export interface IAssetTypeResponse {
    key: string,
    formComponent: string,
    viewComponent: string,
    collectionComponent: string,
    name: string, 
    namePlural: string
}

