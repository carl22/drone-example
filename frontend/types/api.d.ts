import { BvToast } from 'bootstrap-vue';
import Vue from 'vue'
import { AssetService } from '~/plugins/apiService/asset';
import { CountryService } from '~/plugins/apiService/country';
import { MissionService } from '~/plugins/apiService/mission';
import { AuthService } from '../plugins/apiService/auth';

export interface IApiService {
    auth() : AuthService
    country() : CountryService
    mission() : MissionService
    asset() : AssetService
}

export interface IApiQueryParam {
  name: string,
  value: string
}

export interface IApiTransport {
  get(uri: string, params?: Array<IApiQueryParam>) : Promise<any>
  post(uri: string, data: any) : Promise<any>,
  put(uri: string, data: any) : Promise<any>,
  delete(uri: string, params?: Array<IApiQueryParam>) : Promise<any>
}

interface IApiError {
  value: string,
  key?: string
}

export interface IApiErrorHandlerFactory {
  factory() : IApiErrorHandlerReponse
}

export interface IApiErrorHandlerReponse {
  code: number
  errors: Array<IApiError>
  message: string

  processResponse(data: any) : this
  setCode(code: number) : this
  appendError(value: string, key?: string) : this
  getErrors() : Array<IApiError>
  getErrorsForKey(key: string): Array<IApiError>
}

declare module 'vue/types/vue' {
    interface Vue {
      $api: IApiService,
      bvToast: BvToast
    }
  }
  

declare module 'vuex/types/index' {
    interface Store<S> {
      $api: IApiService
    }
  }
  
  declare module '@nuxt/vue-app' {
    interface Context {
      $api: IApiService
    }
    interface NuxtAppOptions {
      $api: IApiService
    }
  }

  declare module '@nuxt/types' {
    interface Context {
      $api: IApiService
    }
  
    interface NuxtAppOptions {
      $api: IApiService
    }
  
  }