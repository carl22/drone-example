import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { IMissionData } from '~/types/forms'
import { find, findIndex, without } from 'lodash'
import Vue from 'vue'

interface tabIndex {
  missionId: number,
  tabIdx: number
}


export const state = () => ({
  missions: [] as IMissionData[],
  tabIndex: []
})

export type RootState = ReturnType<typeof state>

export const getters: GetterTree<RootState, RootState> = {
    byId: state => (id : number) => find(state.missions, {id}),
    tabIndexForMission: state => (missionId : number) => {
      return state.tabIndex[missionId] ?? 0
    }
  }

export const mutations: MutationTree<RootState> = {
  setMissions: (state, missions: Array<IMissionData>) => (state.missions = missions),
  appendMission: (state, mission: IMissionData) => (state.missions.push(mission)),
  replaceMission: (state, mission: IMissionData) => {
    const idx = findIndex(state.missions, {id: mission.id })
    if (idx !== undefined) {
        Vue.set(state.missions,idx, mission)
    }
  },
  removeMission: (state, mission: IMissionData) => state.missions = without(state.missions, mission),
  setMissionTabIndex: (state, idx: tabIndex) =>{
    Vue.set(state.tabIndex, idx.missionId, idx.tabIdx)
  },
}

export const actions: ActionTree<RootState, RootState> = {
  async fetchMissions({ commit, state }) : Promise<Array<IMissionData>> {
    const missions = await this.$api.mission().all()
        commit('setMissions',missions)  
    return state.missions
  },
}
