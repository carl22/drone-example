import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { ILoginUserResponse } from '~/types/apiResponse'

export const state = () => ({
  user: {} as ILoginUserResponse ,
})

export type RootState = ReturnType<typeof state>

export const getters: GetterTree<RootState, RootState> = {
    loggedIn: state => state.user.id !== undefined,
  }

export const mutations: MutationTree<RootState> = {
  setUser: (state, user: ILoginUserResponse )=> (state.user = user),
}

