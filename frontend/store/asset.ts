import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { IAssetTypeResponse } from '~/types/apiResponse'
import { find } from 'lodash'

export const state = () => ({
  types: [] as IAssetTypeResponse[],
  activeAsset: null
})

export type RootState = ReturnType<typeof state>


export const getters: GetterTree<RootState, RootState> = {
  typeByKey: state => (key : string) => find(state.types, {key})
}


export const mutations: MutationTree<RootState> = {
  setTypes: (state, types: Array<IAssetTypeResponse>) => (state.types = types),
  setActiveAsset: (state, asset: any ) => (state.activeAsset = asset),
}

export const actions: ActionTree<RootState, RootState> = {
  async fetchTypes({ commit, state }) : Promise<Array<IAssetTypeResponse>> {
    if(state.types.length === 0) {
        const types = await this.$api.asset().types()
        commit('setTypes',types)
    }   
    return state.types
  },
}
