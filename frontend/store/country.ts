import { GetterTree, ActionTree, MutationTree } from 'vuex'
import { ICountryResponse } from '~/types/apiResponse'

export const state = () => ({
  countries: [] as ICountryResponse[],
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  setCountries: (state, countries: Array<ICountryResponse>) => (state.countries = countries),
}

export const actions: ActionTree<RootState, RootState> = {
  async fetchCountries({ commit, state }) : Promise<Array<ICountryResponse>> {
    if(state.countries.length === 0) {
        const countries = await this.$api.country().all()
        commit('setCountries',countries)
    }   
    return state.countries
  },
}
