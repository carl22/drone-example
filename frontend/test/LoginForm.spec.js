import { mount } from '@vue/test-utils'
import LoginForm from '@/components/form/Login.vue'
import { createLocalVue } from '@vue/test-utils'
import BootstrapVue from 'bootstrap-vue'


const localVue = createLocalVue()
localVue.use(BootstrapVue)


describe('LoginForm', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(LoginForm,{localVue, props: {
      saving: false
    }})
    expect(wrapper.vm).toBeTruthy()
  })
  test('can get form data', () => {
    const testData = {
      email: 'carl@test.com',
      password: 'password'
    }
    const wrapper = mount(LoginForm,{localVue, props: {
      saving: false
    }})
    wrapper.setData(testData)
    wrapper.trigger('submit')
    
    expect(wrapper.emitted().submit[0][0]).toEqual(testData)
  })
})
