import { Context } from "@nuxt/types"
import { Inject } from "@nuxt/types/app"
import { IApiService, IApiTransport, IApiErrorHandlerFactory } from '../types/api'
import { LaravelErrorFactory } from "./apiErrorHandler/laravelErrorHandler"
import { AssetService } from "./apiService/asset"
import { AuthService } from "./apiService/auth"
import { CountryService } from "./apiService/country"
import { MissionService } from "./apiService/mission"
import { AxiosTransport } from "./apiTransport/AxiosTransport"

class ApiService implements IApiService{
    transport: IApiTransport
    constructor(transport: IApiTransport,) {
        this.transport = transport
    }
    auth(): AuthService {
        return new AuthService(this.transport)
    }
    country(): CountryService {
        return new CountryService(this.transport)
    }
    mission(): MissionService {
        return new MissionService(this.transport)
    }
    asset(): AssetService {
        return new AssetService(this.transport)
    }
}


export default (context: Context, inject: Inject) => {
    inject('api', new ApiService(new AxiosTransport(context.$axios, new LaravelErrorFactory())))
}