import { NuxtAxiosInstance } from "@nuxtjs/axios";
import { IApiTransport, IApiErrorHandlerFactory, IApiQueryParam } from "../../types/api";


export class AxiosTransport implements IApiTransport {
    axios: NuxtAxiosInstance
    hasRequestedCSRF = false
    errorFactory: IApiErrorHandlerFactory
    constructor(axios: NuxtAxiosInstance, errorFactory: IApiErrorHandlerFactory) {
        this.axios = axios
        this.errorFactory = errorFactory
    }

    async get(uri: string, params?: Array<IApiQueryParam>) : Promise<any>{
        try {
            const response = await this.axios.get(uri);
            return response.data
        } catch(e: any) {
            throw this.errorFactory.factory().processResponse(e.response)
        }
    }

    async post(uri:string, data: any) : Promise<any>{
        try {
            await this.handleCSRFToken()
            const response = await this.axios.post(uri, data);
            return response.data
        } catch(e: any) {
            throw this.errorFactory.factory().processResponse(e.response)
        }
    }

    async put(uri:string, data: any) : Promise<any>{
        try {
            await this.handleCSRFToken()
            const response = await this.axios.put(uri, data);
            return response.data
        } catch(e: any) {
            throw this.errorFactory.factory().processResponse(e.response)
        }
    }

    async delete(uri: string, params?: Array<IApiQueryParam>) : Promise<any>{
        try {
            await this.handleCSRFToken()
            const response = await this.axios.delete(uri);
            return response.data
        } catch(e: any) {
            throw this.errorFactory.factory().processResponse(e.response)
        }
    }

    protected async handleCSRFToken() : Promise<any>{
        if(this.hasRequestedCSRF === false) {
            await this.axios.get('/sanctum/csrf-cookie')
            this.hasRequestedCSRF = true
        }
    }
    protected handleError(e: any) {
        console.log('errhandler',e.response.status, e.response.data)
    }
}