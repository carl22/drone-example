import { IApiError, IApiErrorHandlerFactory, IApiErrorHandlerReponse } from "~/types/api";

class ApiError implements IApiError {
    value: string
    key?: string
    constructor(value: string, key?: string) {
        this.value = value
        if(key) {
            this.key = key
        }
    }
}

export class LaravelErrorFactory implements IApiErrorHandlerFactory {
    factory() : IApiErrorHandlerReponse {
        return new LaravelErrorResponse()
    }
}

export class LaravelErrorResponse extends Error implements IApiErrorHandlerReponse {
    code: number = 0
    errors: Array<IApiError> = []
  
    processResponse(data: any) : this {
        this.message = 'Unknown Error Occurred'
        if(data.status) {
            this.setCode(data.status)
        }
        if(data.data) {
            if(data.data.message) {
                this.message = data.data.message
            }
            if(data.data.error) {
                this.message = data.data.error
                this.appendError(this.message, '_general')
            }
        } 
        if(data.data && data.data.errors) {
            const keys = Object.keys(data.data.errors)
            keys.forEach(k => {
                const errors : Array<string> = data.data.errors[k]
                errors.forEach(message => this.appendError(message, k))
            })
        }
        return this
    }

    setCode(code: number) : this {
        this.code = code
        return this
    }
    appendError(value: string, key?: string) : this {
        this.errors.push(new ApiError(value, key))
        return this
    }
    getErrors() : Array<IApiError> {
        return this.errors
    } 
    getErrorsForKey(key: string) : Array<IApiError> {
      return this.errors.filter(error => error.key === key)
    }
}