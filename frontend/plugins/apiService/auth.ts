import { ILoginUserResponse } from "~/types/apiResponse";
import { ILoginData, IRegisterData } from "~/types/forms";
import { BaseApiService } from "./base";

class LoginUser implements ILoginUserResponse {
    email: string
    name: string
    id: number
    company: string
    constructor(responseData: any) {
        this.email = responseData.data.email
        this.name = responseData.data.name
        this.id = responseData.data.id
        this.company = responseData.data.company
    }
}

export class AuthService extends BaseApiService {

    async login(data: ILoginData) : Promise<ILoginUserResponse> {
        return new LoginUser(await this.transport.post('/api/login', data)) 
    }
    async register(data: IRegisterData) : Promise<ILoginUserResponse> {
        return new LoginUser(await this.transport.post('/api/register', data)) 
    }
}