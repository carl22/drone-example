import { IMissionData } from "~/types/forms";
import { BaseApiService } from "./base";


export class MissionService extends BaseApiService {

    async all() : Promise<Array<IMissionData>> {
        const missions : Array<IMissionData> = (await this.transport.get('/api/mission')).data
        return missions
    }

    async create(data: IMissionData) : Promise<IMissionData> {
        const mission : IMissionData= (await this.transport.post('/api/mission', data)).data
        return mission
    }

    async update(data: IMissionData) : Promise<IMissionData> {
        const mission : IMissionData= (await this.transport.put('/api/mission/' + data.id, data)).data
        return mission
    }

    async delete(data: IMissionData) : Promise<IMissionData> {
        const mission : IMissionData= (await this.transport.delete('/api/mission/' + data.id)).data
        return mission
    }
}