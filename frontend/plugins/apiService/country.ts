import { ICountryResponse, ILoginUserResponse } from "~/types/apiResponse";
import { BaseApiService } from "./base";


export class CountryService extends BaseApiService {

    async all() : Promise<Array<ICountryResponse>> {
        const countries : Array<ICountryResponse> = (await this.transport.get('/api/country')).data
        return countries
    }
}