import { IApiErrorHandlerFactory, IApiTransport } from "../../types/api"

export abstract class BaseApiService {
    transport: IApiTransport
    constructor(transport: IApiTransport) {
        this.transport = transport
    }
}