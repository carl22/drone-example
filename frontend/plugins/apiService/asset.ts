import { IAssetTypeResponse, ICountryResponse, ILoginUserResponse } from "~/types/apiResponse";
import { BaseApiService } from "./base";


export class AssetService extends BaseApiService {

    async types() : Promise<Array<IAssetTypeResponse>> {
        const types : Array<IAssetTypeResponse> = (await this.transport.get('/api/asset/$types')).data
        return types
    }

    async forMission(type: string, missionId: number) : Promise<any> {
        return (await this.transport.get('/api/mission/'+missionId+'/asset/'+type)).data
    }

    async create(type: string, data: any) : Promise<any> {
        return (await this.transport.post('/api/asset/'+type, data)).data
    }
    async update(id: number, data: any) : Promise<any> {
        return (await this.transport.post('/api/asset/'+id+'/update', data)).data
    }
    async delete(id: number) : Promise<any> {
        return (await this.transport.delete('/api/asset/'+id)).data
    }
}